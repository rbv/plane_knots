#include <vector>
#include <list>
#include <iostream>
#include <algorithm>
#include <set>
#include <cstring>
#include <map>
//#include <cstdlib>
using namespace std;
#include "knot.h"
#include "homflypt.h"
#include "surface.h"
typedef unsigned long long int Bits;

int num_codes, cur_code;
vector<int> gauss_code;
//vector<vector<int> > gauss_code;
int *crossing_sign;
unsigned int CROSSINGS;

//keys polynomials to a list of knots with that polynomial
map<polynomial, list<knotdata> > knot_table;
map<string, bool> found;


int next_code() {
  unsigned int current = CROSSINGS - 1;
  if (num_codes == 0) {
    current = 1;
    if (CROSSINGS > 0) {
    gauss_code[0] = 1;
    gauss_code[1] = 1;
    }
  }
  while (current > 0) {
  
    for (unsigned int i=0; i<CROSSINGS*2; ++i)
      if (gauss_code[i] > current)
	gauss_code[i] = 0;

    //find previous point (to start after)
    int s;
    for (s = CROSSINGS*2-1; s>=0; --s) {
      if (gauss_code[s] == current) {
	gauss_code[s] = 0;
	break;
      }
    }

  next_crossing:
    for (unsigned int j=s+2; j<CROSSINGS*2; j+=2) {
      if (gauss_code[j]==0) {
	gauss_code[j]=current;
	current+=1;
	if (current > CROSSINGS) {
	  ++num_codes;
	  return 1;
	}
	for (s=0; s<CROSSINGS*2; ++s) {
	  if (gauss_code[s] == 0) {
	    gauss_code[s] = current;
	    ++s;
	    goto next_crossing;
	  }
	}
      }
    }

    --current;
  }

  return 0;
}
/*

void generate_codes(int current, vector<int> code)
{
  if (current>CROSSINGS)
  {
    gauss_code.resize(num_codes+1);
    for (vector<int>::iterator it = code.begin(); it!=code.end(); ++it)
    {
      gauss_code[num_codes].push_back(*it);
    }
    ++num_codes;
    return;
  }
  for (unsigned int i=0; i<CROSSINGS*2; ++i)
  {
    if (code[i]==0) {
      code[i]=current;
      for (unsigned int j=i+3; j<CROSSINGS*2; ++j)
      {
	if (code[j]==0 && (j-i)%2==1)
	{
	  code[j]=current;
	  generate_codes(current+1, code);
	  code[j]=0;
	}
      }
      return;
    }
  }
}

*/
int main()
{
  unsigned int i;

  //loop over crossing numbers
  for (CROSSINGS=0; CROSSINGS<=MAX_CROSSING_NUMBER; ++CROSSINGS)
  {
    cout << "crossing number " << CROSSINGS << endl;
      
    //recursively generate all valid unsigned gauss codes
    gauss_code.clear();
    num_codes=0;
    for (i=0; i<CROSSINGS*2; ++i) gauss_code.push_back(0);

    int numsimple = 0;
    crossing_sign = new int[CROSSINGS+1];

    //    for (cur_code=0; cur_code<num_codes; ++cur_code) {

    while (next_code()) {
      /*
      for (i=0; i<CROSSINGS*2; ++i) 
	cout << gauss_code[i] << " ";
      cout << endl;
      */
    }
    cout << "finished generating " << num_codes << " diagrams" << endl;
    /*
      
    //iterate over all the codes we just generated
    for (cur_code=0; cur_code<num_codes; ++cur_code)
    {
      //just output progress as we go, every 100 diagrams
      if (cur_code>0 && cur_code%10000==0) cout << cur_code << endl;
	  
      //check for a simple surface, and planarity
	  
      //this stores whether crossings are met from left or right
      //which is necessary for homfly calculations
      //so this has to be run before those
	  
      //
      bool result = simple_surface(true);
      numsimple += result;

      */

    delete[] crossing_sign;
  }


}

/*
int main()
{
  unsigned int i;
  string name;
  polynomial one=polynomial();
  
  //input knot table
  knot_table[one].push_back(knotdata("0_triv", 0, polynomial()));
  found["0_triv"]=1;
  while (cin >> name)
  {
    int c;
    polynomial p;
    cin >> c;
    p = polynomial::input(&cin);
    knot_table[p].push_back(knotdata(name, c, p));
  }
  
  //loop over crossing numbers
  for (CROSSINGS=0; CROSSINGS<=MAX_CROSSING_NUMBER; ++CROSSINGS)
  {
    cout << "crossing number " << CROSSINGS << endl;
      
    //recursively generate all valid unsigned gauss codes
    gauss_code.clear();
    num_codes=0;
    vector<int> codes;
    for (i=0; i<CROSSINGS*2; ++i) codes.push_back(0);
    generate_codes(1, codes);
    cout << "finished generating " << gauss_code.size() << " diagrams" << endl;

    int numsimple = 0;
    crossing_sign = new int[CROSSINGS+1];

    //continue;
      
    //iterate over all the codes we just generated
    for (cur_code=0; cur_code<num_codes; ++cur_code)
    {


      for (i=0; i<CROSSINGS*2; ++i) 
	cout << gauss_code[cur_code][i] << " ";
      cout << endl;
    continue;

      //just output progress as we go, every 100 diagrams
      if (cur_code>0 && cur_code%10000==0) cout << cur_code << endl;
	  
      //check for a simple surface, and planarity
	  
      //this stores whether crossings are met from left or right
      //which is necessary for homfly calculations
      //so this has to be run before those
	  
      //
      bool result = simple_surface(true);
      numsimple += result;
      */ /*
      
      continue;
	  
      //if diagram is planar and admits a simple seifert surface
      if (result)
      {
	//iterate over all possible assignations of sign
	//(n-bit binary numbers from 1 to 2^n-2)
	Bits max = (((Bits)1) << CROSSINGS);
	for (Bits signs = 0; signs < max; ++signs)
	{
	  //assign crossing signs to gauss code
	  list<pair<int, bool> > knot;
	  bool *visited = new bool[CROSSINGS+1];
	  memset(visited, 0, sizeof(bool)*(CROSSINGS+1));
	  for (vector<int>::iterator it=gauss_code[cur_code].begin();
	       it!=gauss_code[cur_code].end(); ++it)
	  {
	    Bits bit = (((Bits)1) << (*it-1));
	    int sign = ((signs&bit)?(-1):(1));
	    knot.push_back(pair<int, bool>((*it)*sign*((visited[*it])?(-1):(1)),
					   (sign*crossing_sign[*it]>0)));
	    visited[*it]=1;
	  }
	  delete[] visited;
				  
	  //compute homflypt polynomial
	  polynomial p = calculate_polynomial(knot);
				  
	  //find set of possible knots
	  list<knotdata> possible_knots=knot_table[p];
	  int possibilities=0;
	  for (list<knotdata>::iterator it = possible_knots.begin();
	       it!=possible_knots.end(); ++it)
	  {
	    if (it->c<=CROSSINGS) ++possibilities;
	  }
	  if (possibilities==0)
	    ;//cout << "error: knot not in table" << endl;
	  else if (possibilities>1)
	    cout << "error: multiple possible knots" << endl;
	  else {
	    knotdata k = *(possible_knots.begin());
	    if (!found[k.n]) //not previously found
	    {
	      found[k.n]=1;
	      //output data
	      cout << k.n << " d=" << (CROSSINGS-k.c) << " diagram:";
	      //output diagram
	      for (list<pair<int, bool> >::iterator it=knot.begin();
		   it!=knot.end(); ++it)
	      {
		cout << " " << it->first;
	      }
	      cout << endl;
	    }
	  }
	}
      }
      delete[] crossing_sign;

*/ /*
    }
    delete[] crossing_sign;
    cout << numsimple << " were simple." << endl;
  }
}

   */
