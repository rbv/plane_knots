enum border_type {
  BACK_END,
  FRONT_END,
  TOP_STRAND,
  BOTTOM_STRAND,
  ODD_CONNECTION,
  EVEN_CONNECTION,
  GOOD_CONNECTION,
  BAD_CONNECTION,
  ALREADY_GRAPHED
};

struct border_entry {
  border_type type;
  int crossing_num;
  border_entry(border_type t, int c) {
    type=t;
    crossing_num=c;
  }
};

bool knot_search(int i_current, bool *visited,
		 std::vector<std::list<border_entry> > regions, bool surface);
bool simple_surface(bool surface);
