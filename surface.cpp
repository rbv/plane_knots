#include <vector>
#include <list>
#include <iostream>
#include <set>
#include <cstdlib>
#include <cstring>
using namespace std;
#include "knot.h"
#include "surface.h"

bool bad_crossing_found;
void graph_search(int *graph, size_t size, int *visited, int currentNode) {
  if (visited[currentNode]) return;
  visited[currentNode] = 1;

  for (unsigned int i=0; i<size; i++) {
    if (graph[currentNode*size+i]!=0)
      graph_search(graph, size, visited, i);
    if (graph[currentNode*size+i]<0) bad_crossing_found=1;
  }
}

void cut(vector<list<border_entry> >& regions, list<border_entry>::iterator
	 first, list<border_entry>::iterator second,
	 vector<list<border_entry> >::iterator front_region,
	 int crossing, bool top) {
  list<border_entry> new_region;
  list<border_entry>::iterator temp1=first, temp2=second;
  temp1++; temp2++;
  new_region.splice(new_region.end(), *front_region, temp1, second);

  //store sign of crossing according to most recent cut
  if (top) crossing_sign[crossing]=1;
  else crossing_sign[crossing]=-1;

  front_region->erase(first, temp2);

  regions.push_back(new_region);
  //update new front end and good/bad connections between regions
  if (crossing!=0) {
    for (vector<list<border_entry> >::iterator it = regions.begin();
	 it!=regions.end(); ++it)
    {
      for (list<border_entry>::iterator it2 = it->begin(); it2!=it->end();
	   ++it2)
      {

	if (it2->crossing_num==crossing) {
	  if (it2->type==TOP_STRAND || it2->type==BOTTOM_STRAND) {
	    it2->type=FRONT_END;
	  }

	  if (top){
	    if (it2->type==ODD_CONNECTION) {
	      it2->type=BAD_CONNECTION;
	    } else if (it2->type==EVEN_CONNECTION) {
	      it2->type=GOOD_CONNECTION;
	    }
	  }
	  else {
	    if (it2->type==ODD_CONNECTION) {
	      it2->type=GOOD_CONNECTION;
	    } else if (it2->type==EVEN_CONNECTION) {
	      it2->type=BAD_CONNECTION;
	    }
	  }
	}
      }
    }
  }
}

bool knot_search(int i_current, bool *visited, vector<list<border_entry> >
		 regions, bool surface)
{
  unsigned int i,j;

  for (i=i_current; i<=CROSSINGS*2; ++i) {
    int crossing;
    if (i==CROSSINGS*2) { //sticking back end to front end
      crossing = 0;
    } else { //get next crossing
      crossing = abs(gauss_code[i]);
    }
    //cout << crossing << endl;

    //find the front end in front_region
    list<border_entry>::iterator front;
    vector<list<border_entry> >::iterator front_region;
    int pos2;
    for (front_region=regions.begin(); front_region!=regions.end();
	 ++front_region)
    {
      for (front = front_region->begin(), pos2=0; front!=front_region->end();
	   ++front, ++pos2)
      {
	if (front->type==FRONT_END) goto found_front;
      }
    }
  found_front:
    if (i<CROSSINGS*2 && !visited[crossing-1])
    { //insert new crossing at front
      visited[crossing-1]=1;

      front_region->insert(front, border_entry(ODD_CONNECTION, crossing));
      front_region->insert(front, border_entry(TOP_STRAND, crossing));
      front_region->insert(front, border_entry(EVEN_CONNECTION, crossing));
      ++front;
      front_region->insert(front, border_entry(ODD_CONNECTION, crossing));
      front_region->insert(front, border_entry(BOTTOM_STRAND, crossing));
      front_region->insert(front, border_entry(EVEN_CONNECTION, crossing));
    }
    else
    { //cut region in two, connecting to old crossing
      int pos3, tpos3;
      list<border_entry>::iterator it3, strand, temp1, temp2;
      bool found=0;
      for (it3 = front_region->begin(), tpos3=0; it3!=front_region->end();
	   ++it3, ++tpos3)
      {
	if (it3->crossing_num==crossing &&
	    (it3->type==TOP_STRAND || it3->type==BOTTOM_STRAND ||
	     (it3->type==BACK_END && i==CROSSINGS*2)))
	{
	  if (found) { //branch: there are multiple choices
	    if (pos3>pos2) {//hax to ensure order of strands to be joined
	      temp1=front; temp2=strand;
	    } else {
	      temp1=strand; temp2=front;
	    }
	    vector<list<border_entry> > regions_copy;
	    /*clone regions*/
	    vector<list<border_entry> >::iterator front_copy;
	    int front_index, t1index=-1, t2index=-1, index=0;
	    for (vector<list<border_entry> >::iterator cit=regions.begin();
		 cit!=regions.end(); ++cit, ++index)
	    {
	      list<border_entry> reg;
	      int index2=0;
	      for (list<border_entry>::iterator cit2=cit->begin();
		   cit2!=cit->end(); ++cit2, ++index2)
	      {
		if (cit2->type==FRONT_END) front_index=index;
		reg.push_back(border_entry(cit2->type, cit2->crossing_num));
	      }
	      regions_copy.push_back(reg);
	    }
	    for (front_copy=regions_copy.begin(), index=0; index<front_index;
		 ++front_copy, ++index);

	    list<border_entry>::iterator temp1_copy, temp2_copy;
	    for (temp1_copy=front_copy->begin(); !(temp1_copy->type==temp1->type
						   && temp1_copy->crossing_num==temp1->crossing_num); ++temp1_copy);
	    for (temp2_copy=front_copy->begin(); !(temp2_copy->type==temp2->type
						   && temp2_copy->crossing_num==temp2->crossing_num); ++temp2_copy);
	    /**/
	    /*clone visited*/
	    bool *vis2 = new bool[CROSSINGS];
	    memcpy(vis2, visited, sizeof(bool)*CROSSINGS);
	    /**/

	    cut(regions_copy, temp1_copy, temp2_copy, front_copy, crossing,
		strand->type==TOP_STRAND);
	    bool result=knot_search(i+1, vis2, regions_copy, surface);
	    delete[] vis2;
	    if (result) return 1;
	  }
	  strand=it3;
	  pos3=tpos3;
	  found=1;
	}
      }

      if(!found) //this branch is non-planar
	return 0;

      if (pos3>pos2) {//hax to ensure order of strands to be joined
	temp1=front; temp2=strand;
      } else {
	temp1=strand; temp2=front;
      }

      cut(regions, temp1, temp2, front_region, crossing,
	  strand->type==TOP_STRAND);
    }
  }

  if (!surface) return 1;

  //now construct graph of regions and check each component
  size_t size=regions.size();
  int *graph=(int *)malloc(size*size*sizeof(int));
  memset(graph, 0, size*size*sizeof(int));
  i=0;
  for (vector<list<border_entry> >::iterator region=regions.begin();
       region!=regions.end(); ++region, ++i)
  {
    for (list<border_entry>::iterator corner=region->begin();
	 corner!=region->end(); ++corner)
    {
      if (corner->type==ALREADY_GRAPHED) continue;
      j=i;
      for (vector<list<border_entry> >::iterator region2=region;
	   region2!=regions.end(); ++region2, ++j)
      {
	for (list<border_entry>::iterator corner2=region2->begin();
	     corner2!=region2->end(); ++corner2)
	{
	  if (corner2->type==ALREADY_GRAPHED) continue;
	  if (corner==corner2) continue;
	  if (corner2->crossing_num==corner->crossing_num &&
	      corner2->type==corner->type)
	  {

	    if (corner->type==GOOD_CONNECTION){
	      if (graph[size*i+j]>-1) {
		graph[size*i+j]++;
		graph[size*j+i]++;
	      }
	    } else {
	      graph[size*i+j]=-1;
	      graph[size*j+i]=-1;
	    }

	    corner->type=ALREADY_GRAPHED;
	    corner2->type=ALREADY_GRAPHED;
	    goto found_corner;
	  }
	}
      }
    found_corner:;
    }
  }

  //check if either component is free of bad edges
  int *visited_ = (int *)malloc(size*sizeof(int));
  memset(visited_, 0, size*sizeof(int));
  bad_crossing_found=0;
  graph_search(graph, size, visited_, 0);
  if (!bad_crossing_found) return 1;
  bad_crossing_found=0;
  for (i=0; i<size; i++) {
    if (visited_[i]==0) {
      graph_search(graph, size, visited_, i);
      if (!bad_crossing_found) return 1;
      break;
    }
  }
  return 0;
}

bool simple_surface(bool surface)
{
  bool *visited=new bool[CROSSINGS];
  memset(visited, 0, sizeof(bool)*CROSSINGS);

  vector<list<border_entry> > regions;

  list<border_entry> outside;
  outside.push_back(border_entry(BACK_END, 0));
  outside.push_back(border_entry(FRONT_END, 0));
  regions.push_back(outside);

  bool result = knot_search(0, visited, regions, surface);
  delete[] visited;
  return result;
}
