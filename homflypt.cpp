#include <vector>
#include <list>
#include <iostream>
#include <set>
#include <cstring>
#include <cstdlib>
using namespace std;

#include "knot.h"
#include "homflypt.h"

polynomial::polynomial()
{
  terms.insert(term(0,0,1));
}

const void polynomial::print()
{
  bool b=0;
  for (set<term>::iterator it = terms.begin(); it!=terms.end(); ++it)
  {
    if(b) cout << " + ";
    b=1;
    cout << "(" << it->c << ")";
    if (it->x!=0) cout << ".a^(" << it->x << ")";
    if (it->y!=0) cout << ".z^(" << it->y << ")";
  }
  cout << endl;
}

const void polynomial::output(ostream *s)
{
  *s << (unsigned int)terms.size();
  for (set<term>::iterator it = terms.begin(); it!=terms.end(); ++it)
  {
    *s << " " << it->c << " " << it->x << " " << it->y;
  }
}

polynomial polynomial::input(istream *s)
{
  multiset<term> v;
  int z, a, b;
  int terms;

  *s >> terms;
  for (int i=0; i<terms; ++i)
  {
    *s >> z >> a >> b;
    v.insert(term(a,b,z));
  }
  return polynomial(v);
}

bool operator<(const term &a, const term &b)
{
  return (a.x==b.x)?(a.y<b.y):(a.x<b.x);
}


polynomial::polynomial(multiset<term> args)
{
  //add together terms of the same type
  terms.clear();
  multiset<term>::iterator it=args.begin(), jt=args.begin();
  for (++it; it!=args.end(); ++it)
  {
    if (it->x == jt->x && it->y == jt->y) jt->c+=it->c;
    else {
      if (jt->c != 0) terms.insert(*jt);
      jt=it;
    }
  }
  if (jt->c != 0) terms.insert(*jt);
}

/*
polynomial::polynomial(multiset<term> args)
{
  //add together terms of the same type
  terms.clear();
  multiset<term>::iterator it=args.begin();//=args.begin();

  if (it->c != 0) terms.insert(*it);//jt = terms.insert(*it).first;
  set<term>::iterator jt=terms.begin();
  for (++it; it!=args.end(); ++it)
  {
    if (it->x == jt->x && it->y == jt->y) jt->c += it->c;
    else {
      if (it->c != 0) terms.insert(*it);
      jt=it;
    }
  }
}
*/

polynomial polynomial::operator+(const polynomial &a) {
  multiset<term> v;
  for (set<term>::iterator i=terms.begin(); i!=terms.end(); ++i) {
    v.insert(*i);
  }
  for (set<term>::const_iterator i=a.terms.begin(); i!=a.terms.end(); ++i) {
    v.insert(*i);
  }

  return polynomial(v); //this sorts and combines like terms
}

polynomial polynomial::operator*(const polynomial &a) {
  multiset<term> v;

  for (set<term>::iterator i=terms.begin(); i!=terms.end(); ++i) {
    for (set<term>::const_iterator j=a.terms.begin(); j!=a.terms.end(); ++j) {
      v.insert(term(i->x+j->x, i->y+j->y, i->c*j->c));
    }
  }

  return polynomial(v);
}

bool operator<(const polynomial &a, const polynomial &b) {
  if (a.terms.size() != b.terms.size()) return (a.terms.size()<b.terms.size());

  set<term>::const_iterator i=a.terms.begin(), j=b.terms.begin();
  for (; i!=a.terms.end(); ++i, ++j) {
    if (i->x!=j->x) return ((i->x)<(j->x));
    if (i->y!=j->y) return ((i->y)<(j->y));
    if (i->c!=j->c) return ((i->c)<(j->c));
  }
  return 0;
}

bool operator==(const polynomial &a, const polynomial &b) {
  if (a.terms.size() != b.terms.size()) return 0;

  set<term>::const_iterator i=a.terms.begin(), j=b.terms.begin();
  for (; i!=a.terms.end(); ++i, ++j) {
    if (i->x!=j->x) return 0;
    if (i->y!=j->y) return 0;
    if (i->c!=j->c) return 0;
  }

  return 1;
}

bool operator!=(const polynomial &a, const polynomial &b) {
  if (a.terms.size() != b.terms.size()) return 1;

  set<term>::const_iterator i=a.terms.begin(), j=b.terms.begin();
  for (; i!=a.terms.end(); ++i, ++j) {
    if (i->x!=j->x) return 1;
    if (i->y!=j->y) return 1;
    if (i->c!=j->c) return 1;
  }
  return 0;
}

polynomial one, a2, a2i, az, aiz, azi_aizi;
void prepare_polynomials() {
  multiset<term> temp;
  temp.insert(term(0,0,1));
  one = polynomial(temp);
  temp.clear();
  temp.insert(term(2,0,1));
  a2 = polynomial(temp);
  temp.clear();
  temp.insert(term(-2,0,1));
  a2i = polynomial(temp);
  temp.clear();
  temp.insert(term(1,1,-1));
  az = polynomial(temp);
  temp.clear();
  temp.insert(term(-1,1,1));
  aiz = polynomial(temp);
  temp.clear();
  temp.insert(term(1,-1,1));
  temp.insert(term(-1,-1,-1));
  azi_aizi = polynomial(temp);
  temp.clear();
}

polynomial calculate_polynomial(list<pair<int, bool> > knot) {
  list<list<pair<int, bool> > > link;
  link.push_back(knot);
  prepare_polynomials();
  polynomial p = calculate_polynomial(link);
  return p;
}

polynomial calculate_polynomial(list<list<pair<int, bool> > > link) {
  bool *visited = new bool[CROSSINGS+1];
  memset(visited, 0, sizeof(bool)*(CROSSINGS+1));

  for (list<list<pair<int, bool> > >::iterator comp=link.begin();
       comp!=link.end(); ++comp)
  {

    for (list<pair<int, bool> >::iterator crossi=comp->begin();
	 crossi!=comp->end(); ++crossi)
    {
      int crossing = abs(crossi->first);
      if (crossi->first>0) {
	visited[crossing] = 1;
      } else if (!visited[crossing]) {
	//undercrossing and haven't met overcrossing yet
	polynomial p;
	//branch!
	//L+ is current
	//make LO from link = L+
	list<list<pair<int, bool> > > l0;

	//find other instance of this crossing
	list<list<pair<int, bool> > >::iterator comp0 = comp;
	list<pair<int, bool> >::iterator under = crossi;
	++crossi;
	for (; crossi!=comp->end(); ++crossi)
	{
	  if (crossi->first == crossing) goto found;
	}
	for (++comp; comp!=link.end(); ++comp)
	{
	  for (crossi=comp->begin(); crossi!=comp->end(); ++crossi)
	  {
	    if (crossi->first == crossing) goto found;
	  }
	}
	cout << "error, should be unreachable" << endl;
	exit(1);
      found:;
	//if in same component => in different now
	if (comp0==comp) {
	  for (list<list<pair<int, bool> > >::iterator it=link.begin();
	       it!=link.end(); ++it)
	  {
	    if (it==comp0) {
	      list<pair<int, bool> > newlist1, newlist2;
	      list<pair<int, bool> >::iterator it2;

	      //up to first instance of crossing
	      for (it2=comp0->begin(); it2!=under; ++it2)
		newlist1.push_back(*it2);

	      //up to second instance of crossing
	      for (++it2; it2!=crossi; ++it2)
		newlist2.push_back(*it2);

	      //back to start
	      for (++it2; it2!=comp0->end(); ++it2)
	      {
		newlist1.push_back(*it2);
	      }
	      l0.push_back(newlist1);
	      l0.push_back(newlist2);
	    } else {
	      list<pair<int, bool> > newlist;
	      for (list<pair<int, bool> >::iterator it2=it->begin();
		   it2!=it->end(); ++it2)
	      {
		newlist.push_back(*it2);
	      }
	      l0.push_back(newlist);
	    }
	  }
	}
	//if in different components => now is one component
	else {
	  for (list<list<pair<int, bool> > >::iterator it=link.begin();
	       it!=link.end(); ++it)
	  {
	    if (it == comp0) {
	      list<pair<int, bool> > newlist;
	      list<pair<int, bool> >::iterator it2, it3;
	      //first component up to crossing
	      for (it2=comp0->begin(); it2!=under; ++it2)
	      {
		newlist.push_back(*it2);
	      }
	      //second component from crossing
	      it3=crossi;
	      for (++it3; it3!=crossi; ++it3)
	      {
		if (it3==comp->end()) {
		  it3=comp->begin();
		  if (it3==crossi) break;
		}
		newlist.push_back(*it3);
	      }
	      for (++it2; it2!=comp0->end(); ++it2) //first component to end
	      {
		newlist.push_back(*it2);
	      }
	      l0.push_back(newlist);
	    } else if (it != comp) {
	      list<pair<int, bool> > newlist;

	      for (list<pair<int, bool> >::iterator it2=it->begin();
		   it2!=it->end(); ++it2)
	      {
		newlist.push_back(*it2);
	      }
	      l0.push_back(newlist);
	    }
	  }
	}
	delete[] visited;
	crossi->first*=-1; //flip crossing
	under->first*=-1;
	crossi->second=!crossi->second;
	under->second=!under->second;
	a2, a2i, az, aiz, azi_aizi;
	p = ((crossi->second)?(az):(aiz)) * calculate_polynomial(l0);
	p = p + ((crossi->second)?(a2):(a2i)) * calculate_polynomial(link);
	//should actually pass current location to shorten computation
	return p;
      }
    }
  }

  delete[] visited;
  //base case
  polynomial p=one;
  for (unsigned int i=0; i<link.size()-1; ++i) p=p*azi_aizi;

  return p;
}


