CPP = g++
CPPFLAGS = -O2 -g -Wall
LIBS = 
OBJECTS = surface.o main.o homflypt.o
BINARY = countk

all: $(OBJECTS)
	$(CPP) $(CPPFLAGS) -o $(BINARY) $(OBJECTS) $(LIBS)

clean:
	rm -f $(OBJECTS)  $(BINARY)

a: clean all
