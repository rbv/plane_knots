extern unsigned int CROSSINGS;
extern int num_codes, cur_code;
//extern std::vector<std::vector<int> > gauss_code;
extern std::vector<int> gauss_code;
extern int *crossing_sign;
#define MAX_CROSSING_NUMBER 14

struct term {
  int x;
  int y;
  mutable int c;
  term(int a, int b, int z) {x=a; y=b; c=z;}
};

class polynomial {
 public:
  std::set<term> terms;

  polynomial();
  polynomial(std::multiset<term> args);
  polynomial operator+(const polynomial &a);
  polynomial operator+=(const polynomial &a);
  polynomial operator*(const polynomial &a);
  polynomial operator*=(const polynomial &a);
  const void print();
  const void output(std::ostream *s);
  static polynomial input(std::istream *s);
};

bool operator<(const term &a, const term &b);
bool operator<(const polynomial &a, const polynomial &b);
bool operator==(const polynomial &a, const polynomial &b);
bool operator!=(const polynomial &a, const polynomial &b);

struct knotdata
{
  string n; //name
  unsigned int c; //crossing number
  polynomial p; //homfly
  knotdata(string name, int cross, polynomial poly){
    n=name; c=cross; p=poly;
  }
};
